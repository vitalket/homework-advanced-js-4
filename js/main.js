/* 
   1. Поясніть своїми словами, що таке AJAX і чим він корисний при розробці Javascript?
      - Це асинхронні запроси на сервер через JS. AJAX дозволяє завантажувати дані без оновлення сторінки
 */

const filmsList = document.querySelector('.film-list');
const preloader = document.querySelector('.preloader-container');

preloader.style.display = 'block';
fetch('https://ajax.test-danit.com/api/swapi/films')
  .then(res => res.json())
  .then(films => {

  preloader.style.display = 'none';

  films.forEach(film => {

    const filmItem = document.createElement('li');

    filmItem.innerHTML = `
      <div>${film.episodeId}</div>
      <h4>${film.name}</h4>
      <ul class="characters-list"></ul>
      <p>${film.openingCrawl}</p>
    `;

    filmsList.appendChild(filmItem);

    const charactersList = filmItem.querySelector('.characters-list');
 
    film.characters.forEach(chURL => {
      fetch(chURL)
        .then(res => res.json())
        .then(person => {

          const characterItem = document.createElement('li');

          for (const key in person) {
              characterItem.innerHTML += `<strong>${key}:</strong> ${person[key]}<br>`;
            }

          charactersList.appendChild(characterItem);

      }).catch(err => {
        preloader.style.display = 'none';
        console.error('Помилка при отриманні інформації про персонажів: ' + err);
      })
    })

  });
  
}).catch(err => {
  preloader.style.display = 'none';
  console.error('Помилка при отриманні інформації про фільми: ' + err);
})
